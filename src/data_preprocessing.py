import pandas as pd
from sklearn.preprocessing import StandardScaler

def handle_missing_values(data: pd.DataFrame) -> pd.DataFrame:
    # Check for missing values
    missing_values = data.isnull().sum()
    
    # Example strategy: Remove rows with any missing values
    data.dropna(inplace=True)
    
    return data

def convert_data_types(data: pd.DataFrame) -> pd.DataFrame:
    # Convert numeric columns
    numeric_cols = ['MRP', 'Discount (%)', 'Quantity', 'Product Rating']
    data[numeric_cols] = data[numeric_cols].apply(pd.to_numeric, errors='coerce')
    
    return data

def handle_date(data: pd.DataFrame) -> pd.DataFrame:
    # Assuming Purchase Date is datetime format
    data['Purchase Date'] = pd.to_datetime(data['Purchase Date'])
    
    # Extract features like month or year if needed
    data['Year'] = data['Purchase Date'].dt.year
    data['Month'] = data['Purchase Date'].dt.month
    
    return data

def calculate_total_sales(data: pd.DataFrame) -> pd.DataFrame:
    data['Total Sales'] = data['Quantity'] * (data['MRP'] - (data['MRP'] * data['Discount (%)'] / 100))
    return data


def calculate_rfm_metrics(data: pd.DataFrame) -> pd.DataFrame:
    # Assuming Purchase Date is datetime format
    data['Purchase Date'] = pd.to_datetime(data['Purchase Date'])
    current_date = pd.to_datetime('now')
    
    # Calculate Recency, Frequency, Monetary metrics
    rfm_metrics = data.groupby('Customer ID').agg({
        'Purchase Date': lambda x: (current_date - x.max()).days,  # Recency
        'Quantity': 'sum',  # Frequency: Total quantity purchased
        'Total Sales': 'sum'  # Monetary: Total sales amount
    }).rename(columns={
        'Purchase Date': 'Recency'
    }).reset_index()
    
    return rfm_metrics

def preprocess_data(input_path: str, output_path: str) -> None:
    # Load data
    data = pd.read_csv(input_path)
    
    # Data cleaning
    data = handle_missing_values(data)
    
    # Convert data types
    data = convert_data_types(data)
    
    # Handle date
    data = handle_date(data)
    
    # Feature engineering
    data = calculate_total_sales(data)

    
    # Calculate RFM metrics (optional)
    rfm_metrics = calculate_rfm_metrics(data)
    
    # Save processed data
    data.to_csv(output_path, index=False)
    
    print(f"Data preprocessing completed. Processed data saved to {output_path}")

if __name__ == "__main__":
    input_path = '../data/raw/sales_data.csv'
    output_path = '../data/processed/processed_sales_data.csv'
    preprocess_data(input_path, output_path)
