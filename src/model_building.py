import pandas as pd
import os
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans, AgglomerativeClustering, DBSCAN
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import silhouette_score
from sklearn.model_selection import GridSearchCV
from sklearn.decomposition import PCA
import seaborn as sns

def read_data(file_path: str) -> pd.DataFrame:
    return pd.read_csv(file_path)

def preprocess_data(df: pd.DataFrame) -> pd.DataFrame:
    scaler = StandardScaler()
    df_scaled = scaler.fit_transform(df.select_dtypes(include=['float64', 'int64']))
    return pd.DataFrame(df_scaled, columns=df.select_dtypes(include=['float64', 'int64']).columns)

def cluster_kmeans(df_scaled: pd.DataFrame, n_clusters: int) -> pd.Series:
    kmeans = KMeans(n_clusters=n_clusters, random_state=42)
    cluster_labels = kmeans.fit_predict(df_scaled)
    return pd.Series(cluster_labels)

def cluster_agglomerative(df_scaled: pd.DataFrame, n_clusters: int) -> pd.Series:
    agglomerative = AgglomerativeClustering(n_clusters=n_clusters)
    cluster_labels = agglomerative.fit_predict(df_scaled)
    return pd.Series(cluster_labels)

def cluster_dbscan(df_scaled: pd.DataFrame, eps_values: list, min_samples_values: list) -> tuple[pd.Series, float]:
    best_silhouette_score = -1
    best_eps = 0
    best_min_samples = 0
    best_labels = None
    
    for eps in eps_values:
        for min_samples in min_samples_values:
            dbscan = DBSCAN(eps=eps, min_samples=min_samples)
            cluster_labels = dbscan.fit_predict(df_scaled)
            
            # Check if more than one cluster is found
            unique_clusters = pd.Series(cluster_labels).nunique()
            
            if unique_clusters > 1:
                silhouette_avg = silhouette_score(df_scaled, cluster_labels)
                
                if silhouette_avg > best_silhouette_score:
                    best_silhouette_score = silhouette_avg
                    best_eps = eps
                    best_min_samples = min_samples
                    best_labels = cluster_labels
    
    return pd.Series(best_labels), best_silhouette_score

def evaluate_cluster_quality(df_scaled: pd.DataFrame, cluster_labels: pd.Series) -> float:
    silhouette_avg = silhouette_score(df_scaled, cluster_labels)
    return silhouette_avg

def plot_clusters_pca(df_scaled: pd.DataFrame, cluster_labels: pd.Series, model_name: str, output_dir: str) -> None:
    pca = PCA(n_components=2)
    principal_components = pca.fit_transform(df_scaled)
    principal_df = pd.DataFrame(data=principal_components, columns=['PC1', 'PC2'])
    principal_df['Cluster'] = cluster_labels
    
    plt.figure(figsize=(10, 6))
    sns.scatterplot(x='PC1', y='PC2', hue='Cluster', data=principal_df, palette='viridis', legend='full')
    plt.title(f'Clusters by {model_name} with PCA')
    plt.savefig(os.path.join(output_dir, f'{model_name}_clusters_pca.png'))
    plt.close()

def plot_metrics(silhouette_scores: dict, output_dir: str) -> None:
    plt.figure(figsize=(8, 6))
    models = list(silhouette_scores.keys())
    scores = list(silhouette_scores.values())
    ax = sns.barplot(x=models, y=scores, palette='viridis')
    plt.title('Silhouette Scores of Clustering Algorithms')
    plt.xlabel('Clustering Algorithm')
    plt.ylabel('Silhouette Score')
    plt.xticks()
    
    # Adding grid lines
    ax.grid(axis='y', linestyle='--')
    
    # Displaying values on the bars
    for i, score in enumerate(scores):
        ax.text(i, score + 0.005, f'{score:.3f}', ha='center', va='bottom', fontsize=9)
    
    plt.tight_layout()
    plt.savefig(os.path.join(output_dir, 'clustering_algorithms_comparison.png'))
    plt.close()

def main() -> None:
    # File paths
    input_data_path = '../data/processed/processed_sales_data.csv'
    output_dir = '../assets/model_evaluation'
    
    # Ensure the output directory exists
    os.makedirs(output_dir, exist_ok=True)
    
    # Read data
    df = read_data(input_data_path)
    
    # Select numerical columns for clustering
    df_scaled = preprocess_data(df)
    
    # Dictionary to store silhouette scores
    silhouette_scores = {}
    
    # Clustering using K-means
    n_clusters_kmeans = 3
    kmeans_labels = cluster_kmeans(df_scaled, n_clusters_kmeans)
    silhouette_kmeans = evaluate_cluster_quality(df_scaled, kmeans_labels)
    silhouette_scores['KMeans'] = silhouette_kmeans
    plot_clusters_pca(df_scaled, kmeans_labels, 'KMeans', output_dir)
    
    # Clustering using Agglomerative
    n_clusters_agglo = 3
    agglo_labels = cluster_agglomerative(df_scaled, n_clusters_agglo)
    silhouette_agglo = evaluate_cluster_quality(df_scaled, agglo_labels)
    silhouette_scores['Agglomerative'] = silhouette_agglo
    plot_clusters_pca(df_scaled, agglo_labels, 'Agglomerative', output_dir)
    
    # Clustering using DBSCAN (with hyperparameter tuning)
    eps_values = [0.5, 1.0, 1.5]
    min_samples_values = [5, 10, 15]
    best_silhouette_dbscan = -1
    best_eps_dbscan = 0
    best_min_samples_dbscan = 0
    
    for eps in eps_values:
        for min_samples in min_samples_values:
            dbscan_labels, silhouette_dbscan = cluster_dbscan(df_scaled, [eps], [min_samples])
            
            if silhouette_dbscan > best_silhouette_dbscan:
                best_silhouette_dbscan = silhouette_dbscan
                best_eps_dbscan = eps
                best_min_samples_dbscan = min_samples
    
    dbscan_labels, _ = cluster_dbscan(df_scaled, [best_eps_dbscan], [best_min_samples_dbscan])
    silhouette_scores['DBSCAN'] = best_silhouette_dbscan
    plot_clusters_pca(df_scaled, dbscan_labels, 'DBSCAN', output_dir)
    
    # Plot silhouette scores comparison
    plot_metrics(silhouette_scores, output_dir)
    
    # Determine the best clustering algorithm
    best_algorithm = max(silhouette_scores, key=silhouette_scores.get)
    print(f"Best Clustering Algorithm: {best_algorithm} with Silhouette Score: {silhouette_scores[best_algorithm]}")
    print(f"Clustering models evaluated and plots saved in '{output_dir}'.")

if __name__ == "__main__":
    main()
