import os
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

# Set the data path and directory for storing visuals
data_path = "../data/processed/processed_sales_data.csv"
output_dir = "../assets/data_insight"

# Ensure the output directory exists
os.makedirs(output_dir, exist_ok=True)

# Load the processed sales data
def load_data(data_path: str) -> pd.DataFrame:
    return pd.read_csv(data_path)

# Plot the discount distribution
def plot_discount_distribution(data: pd.DataFrame) -> None:
    plt.figure(figsize=(10, 6))
    ax = sns.barplot(x='Product', y='Discount (%)', data=data, errorbar=None)
    for p in ax.patches:
        ax.annotate(format(p.get_height(), '.2f'), 
                    (p.get_x() + p.get_width() / 2., p.get_height()), 
                    ha='center', va='center', 
                    xytext=(0, 9), 
                    textcoords='offset points')
    plt.title('Discount Distribution by Product')
    plt.xlabel('Product')
    plt.ylabel('Discount (%)')
    plt.grid(True)
    plt.savefig(os.path.join(output_dir, 'discount_distribution.png'))
    plt.close()

# Plot the product rating distribution
def plot_product_rating_distribution(data: pd.DataFrame) -> None:
    plt.figure(figsize=(10, 6))
    ax = sns.barplot(x='Product', y='Product Rating', data=data, errorbar=None)
    for p in ax.patches:
        ax.annotate(format(p.get_height(), '.2f'), 
                    (p.get_x() + p.get_width() / 2., p.get_height()), 
                    ha='center', va='center', 
                    xytext=(0, 9), 
                    textcoords='offset points')
    plt.title('Product Rating Distribution by Product')
    plt.xlabel('Product')
    plt.ylabel('Product Rating')
    plt.grid(True)
    plt.savefig(os.path.join(output_dir, 'product_rating_distribution.png'))
    plt.close()

# Plot quantity over time
def plot_quantity_over_time(data: pd.DataFrame) -> None:
    plt.figure(figsize=(10, 6))
    data_grouped = data.groupby(['Year', 'Month'])['Quantity'].sum().reset_index()
    data_grouped['Year'] = data_grouped['Year'].astype(int)
    data_grouped['Month'] = data_grouped['Month'].astype(int)
    data_grouped['Date'] = pd.to_datetime(data_grouped.assign(Day=1)[['Year', 'Month', 'Day']])
    ax = sns.lineplot(x='Date', y='Quantity', data=data_grouped)
    for index, row in data_grouped.iterrows():
        ax.annotate(row['Quantity'], 
                    (row['Date'], row['Quantity']), 
                    ha='center', va='center', 
                    xytext=(0, 9), 
                    textcoords='offset points')
    plt.title('Quantity Over Time')
    plt.xlabel('Time')
    plt.ylabel('Quantity')
    plt.grid(True)
    plt.savefig(os.path.join(output_dir, 'quantity_over_time.png'))
    plt.close()

# Plot total sales over time
def plot_total_sales_over_time(data: pd.DataFrame) -> None:
    plt.figure(figsize=(10, 6))
    data_grouped = data.groupby(['Year', 'Month'])['Total Sales'].sum().reset_index()
    data_grouped['Year'] = data_grouped['Year'].astype(int)
    data_grouped['Month'] = data_grouped['Month'].astype(int)
    data_grouped['Date'] = pd.to_datetime(data_grouped.assign(Day=1)[['Year', 'Month', 'Day']])
    ax = sns.lineplot(x='Date', y='Total Sales', data=data_grouped)
    for index, row in data_grouped.iterrows():
        ax.annotate(round(row['Total Sales'], 2), 
                    (row['Date'], row['Total Sales']), 
                    ha='center', va='center', 
                    xytext=(0, 9), 
                    textcoords='offset points')
    plt.title('Total Sales Over Time')
    plt.xlabel('Time')
    plt.ylabel('Total Sales')
    plt.grid(True)
    plt.savefig(os.path.join(output_dir, 'total_sales_over_time.png'))
    plt.close()

# Plot consumers by location
def plot_consumers_by_location(data: pd.DataFrame) -> None:
    plt.figure(figsize=(10, 6))
    data_grouped = data['Location'].value_counts().reset_index()
    data_grouped.columns = ['Location', 'Count']  # Rename columns for clarity
    ax = sns.barplot(x='Location', y='Count', data=data_grouped, errorbar=None)
    for p in ax.patches:
        ax.annotate(format(p.get_height(), '.0f'), 
                    (p.get_x() + p.get_width() / 2., p.get_height()), 
                    ha='center', va='center', 
                    xytext=(0, 9), 
                    textcoords='offset points')
    plt.title('Consumers by Location')
    plt.xlabel('Location')
    plt.ylabel('Number of Consumers')
    plt.grid(True)
    plt.savefig(os.path.join(output_dir, 'consumers_by_location.png'))
    plt.close()

# Plot gender count
def plot_gender_count(data: pd.DataFrame) -> None:
    plt.figure(figsize=(10, 6))
    ax = sns.countplot(x='Gender', data=data)
    for p in ax.patches:
        ax.annotate(format(p.get_height(), '.0f'), 
                    (p.get_x() + p.get_width() / 2., p.get_height()), 
                    ha='center', va='center', 
                    xytext=(0, 9), 
                    textcoords='offset points')
    plt.title('Gender Count')
    plt.xlabel('Gender')
    plt.ylabel('Count')
    plt.grid(True)
    plt.savefig(os.path.join(output_dir, 'gender_count.png'))
    plt.close()

# Plot payment distribution as a donut chart
def plot_payment_distribution(data: pd.DataFrame) -> None:
    plt.figure(figsize=(10, 6))
    payment_counts = data['Payment Method'].value_counts()
    wedges, texts, autotexts = plt.pie(payment_counts, labels=[f'{name}' for name in payment_counts.index],
                                       autopct=lambda pct: f'{pct:.2f}% ({int(round(pct * sum(payment_counts) / 100.0))})',
                                       startangle=140, wedgeprops=dict(width=0.3))
    for i, text in enumerate(texts):
        text.set_text(f'{text.get_text()} ({payment_counts.iloc[i]})')
    plt.title('Payment Distribution')
    plt.axis('equal')
    plt.savefig(os.path.join(output_dir, 'payment_distribution.png'))
    plt.close()

# Plot product by gender
def plot_product_by_gender(data: pd.DataFrame) -> None:
    plt.figure(figsize=(10, 6))
    ax = sns.countplot(x='Product', hue='Gender', data=data)
    for p in ax.patches:
        ax.annotate(format(p.get_height(), '.0f'), 
                    (p.get_x() + p.get_width() / 2., p.get_height()), 
                    ha='center', va='center', 
                    xytext=(0, 9), 
                    textcoords='offset points')
    plt.title('Product by Gender')
    plt.xlabel('Product')
    plt.ylabel('Count')
    plt.grid(True)
    plt.savefig(os.path.join(output_dir, 'product_by_gender.png'))
    plt.close()

# Plot product by location
def plot_product_by_location(data: pd.DataFrame) -> None:
    plt.figure(figsize=(10, 6))
    ax = sns.countplot(x='Product', hue='Location', data=data)
    for p in ax.patches:
        ax.annotate(format(p.get_height(), '.0f'), 
                    (p.get_x() + p.get_width() / 2., p.get_height()), 
                    ha='center', va='center', 
                    xytext=(0, 9), 
                    textcoords='offset points')
    plt.title('Product by Location')
    plt.xlabel('Product')
    plt.ylabel('Count')
    plt.grid(True)
    plt.savefig(os.path.join(output_dir, 'product_by_location.png'))
    plt.close()

# Plot total sales by gender
def plot_total_sales_by_gender(data: pd.DataFrame) -> None:
    plt.figure(figsize=(10, 6))
    data_grouped = data.groupby('Gender')['Total Sales'].sum().reset_index()
    ax = sns.barplot(x='Gender', y='Total Sales', data=data_grouped, errorbar=None)
    for p in ax.patches:
        ax.annotate(format(p.get_height(), '.2f'), 
                    (p.get_x() + p.get_width() / 2., p.get_height()), 
                    ha='center', va='center', 
                    xytext=(0, 9), 
                    textcoords='offset points')
    plt.title('Total Sales by Gender')
    plt.xlabel('Gender')
    plt.ylabel('Total Sales')
    plt.grid(True)
    plt.savefig(os.path.join(output_dir, 'total_sales_by_gender.png'))
    plt.close()

# Generate all plots
def generate_all_plots(data_path: str) -> None:
    data = load_data(data_path)
    plot_discount_distribution(data)
    plot_product_rating_distribution(data)
    plot_quantity_over_time(data)
    plot_total_sales_over_time(data)
    plot_consumers_by_location(data)
    plot_gender_count(data)
    plot_payment_distribution(data)
    plot_product_by_gender(data)
    plot_product_by_location(data)
    plot_total_sales_by_gender(data)

# Run the script to generate all plots
if __name__ == "__main__":
    generate_all_plots(data_path)
