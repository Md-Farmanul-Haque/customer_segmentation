import pandas as pd
import os
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import silhouette_score
from sklearn.decomposition import PCA
import seaborn as sns

def read_data(file_path: str) -> pd.DataFrame:
    return pd.read_csv(file_path)

def preprocess_data(df: pd.DataFrame) -> pd.DataFrame:
    scaler = StandardScaler()
    df_scaled = scaler.fit_transform(df.select_dtypes(include=['float64', 'int64']))
    return pd.DataFrame(df_scaled, columns=df.select_dtypes(include=['float64', 'int64']).columns)

def evaluate_kmeans(df_scaled: pd.DataFrame, n_clusters: int) -> tuple[pd.Series, float]:
    kmeans = KMeans(n_clusters=n_clusters, random_state=42)
    cluster_labels = kmeans.fit_predict(df_scaled)
    silhouette_avg = silhouette_score(df_scaled, cluster_labels)
    return pd.Series(cluster_labels), silhouette_avg

def plot_clusters_pca(df_scaled: pd.DataFrame, cluster_labels: pd.Series, model_name: str, output_dir: str) -> None:
    pca = PCA(n_components=2)
    principal_components = pca.fit_transform(df_scaled)
    principal_df = pd.DataFrame(data=principal_components, columns=['PC1', 'PC2'])
    principal_df['Cluster'] = cluster_labels
    
    plt.figure(figsize=(10, 6))
    sns.scatterplot(x='PC1', y='PC2', hue='Cluster', data=principal_df, palette='viridis', legend='full')
    plt.title(f'Clusters by {model_name} with PCA')
    plt.savefig(os.path.join(output_dir, f'{model_name}_clusters_pca.png'))
    plt.close()

def main() -> None:
    # File paths
    input_data_path = '../data/processed/processed_sales_data.csv'
    output_dir = '../assets/model_evaluation'
    
    # Ensure the output directory exists
    os.makedirs(output_dir, exist_ok=True)
    
    # Read data
    df = read_data(input_data_path)
    
    # Select numerical columns for clustering
    df_scaled = preprocess_data(df)
    
    # Optimized number of clusters from model_optimisation.py
    optimized_n_clusters = 3
    
    # Evaluate K-means clustering
    kmeans_labels, silhouette_kmeans = evaluate_kmeans(df_scaled, optimized_n_clusters)
    
    # Plot clusters using PCA
    plot_clusters_pca(df_scaled, kmeans_labels, f'KMeans_optimized_{optimized_n_clusters}_clusters', output_dir)
    
    # Output results
    print(f"Optimized KMeans Clustering with {optimized_n_clusters} clusters achieved Silhouette Score: {silhouette_kmeans:.3f}")
    print(f"Plots saved in '{output_dir}'.")

if __name__ == "__main__":
    main()
