# Customer Segmentation Project

This project focuses on customer segmentation using clustering techniques. We employ the K-Means algorithm after experimenting with various clustering techniques. Future updates will include optimization methods to enhance the clustering process.

## Table of Contents

- [Objective](#objective)

- [Project Structure](#project-structure)

- [Data](#data)


- [Dependencies](#dependencies)

- [Usage](#usage)

- [Results](#results)

- [License](#license)

- [Contact](#contact)

## Objective

The objective of this project is to segment customers based on their purchasing behavior, demographics, and other relevant factors. The insights derived from the segmentation will help in targeted marketing strategies and improving customer retention.

## Project Structure

```

customer_segmentation/
├── assets/
│   ├── data_report/
│   ├── miscellaneous/
│   ├── model_report/
│   └── visual_insights/
│
├── dashboards/
│   ├── customer_segmentation_dashboard.pbix
│   └── dashboard_images/
│
├── data/
│   ├── raw/
│   │   └── sales_data.csv
│   └── processed/
│       └── cleaned_sales_data.csv
│
├── src/
│   ├── preprocessing.py
│   ├── clustering.py
│   ├── visualization.py
│   └── optimization.py
│
├── LICENSE
├── README.md
└── requirements.txt

```

## Data

The dataset includes customer details such as purchase history, demographics, and product ratings. Here is a sample of the data:

```
Customer ID
Purchase Date
Location
Product
Color
MRP
Discount (%)
Gender
Payment Method
Quantity
Product Rating
```

## Dependencies

The project requires the following Python libraries:

- pandas
- numpy
- scikit-learn
- matplotlib
- seaborn

You can install the dependencies using the following command:

```bash

pip install -r requirements.txt

```

## Usage

1. **Preprocess the data:**
   ```python

   python src/preprocessing.py
   
   ```

2. **Run the clustering algorithm:**
   ```python
   
   python src/clustering.py
   
   ```

3. **Visualize the results:**
   ```python
   
   python src/visualization.py
   
   ```

4. **Optimization (future updates):**
   ```python
   
   python src/optimization.py
   
   ```

## Results

The results of the clustering will be saved in the `assets` directory, including data reports, model evaluation metrics, and visual insights. Additionally, a Power BI dashboard will provide interactive visualizations of the customer segments.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

## Contact

- **Name:** Md Farmanul Haque

- **Email:** [farmanhaque74@gmail.com](mailto:farmanhaque74@gmail.com)

- **GitLab:** [Md-Farmanul-Haque](https://gitlab.com/Md-Farmanul-Haque)

- **Project Repository:** [Customer Segmentation](https://gitlab.com/Md-Farmanul-Haque/customer_segmentation.git)
